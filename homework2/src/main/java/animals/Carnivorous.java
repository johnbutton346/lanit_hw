package animals;

import food.Food;
import food.Grass;
import food.Meat;

public class Carnivorous extends Animal {

    public Carnivorous() {}

    public Carnivorous(int satiety, String type) {
        super(satiety, type);
    }

    @Override
    public void eatFood(Food food) {
        if (food instanceof Grass) {
            System.out.printf("%s is carnivorous and can't eat %s because it's grass!\n", this.getType(), food.getName());
        } else {
            this.setSatiety(this.getSatiety() + food.getNourishment());
            System.out.printf("%s eating %s... Satiety value now is %d\n", this.getType(), food.getName(), this.getSatiety());
        }
    }
}
