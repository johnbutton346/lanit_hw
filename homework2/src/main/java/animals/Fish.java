package animals;

public class Fish extends Carnivorous implements Swim {

    public Fish() {
    }

    public Fish(int satiety, String type) {
        super(satiety, type);
    }

    @Override
    public void swim() {
        System.out.printf("The %s is swim... he can only swim.. so sad :((\n", getType());
    }
}
