package animals;

import food.Food;
import food.Grass;
import food.Meat;

public class Herbivore extends Animal {

    public Herbivore() {}

    public Herbivore(int satiety, String type) {
        super(satiety, type);
    }

    @Override
    public void eatFood(Food food) {
        if (food instanceof Meat) {
            System.out.printf("%s is herbivore and can't eat %s because it's meat!\n", this.getType(), food.getName());
        } else {
            this.setSatiety(this.getSatiety() + food.getNourishment());
            System.out.printf("%s eating %s... Satiety value now is %d.\n", this.getType(), food.getName(), this.getSatiety());
        }
    }
}
