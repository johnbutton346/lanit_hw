package animals;

public class Bear extends Carnivorous implements Run, Swim, Voice {

    public Bear() { }

    public Bear(int satiety, String type) {
        super(satiety, type);
    }

    @Override
    public void run() {
        System.out.printf("The %s is running!\n", this.getType());
    }

    @Override
    public void swim() {
        System.out.printf("The %s is swim.\n", this.getType());
    }

    @Override
    public String voice() {
        return "RAAWR!";
    }
}
