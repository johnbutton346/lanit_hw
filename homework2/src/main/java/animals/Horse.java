package animals;

public class Horse extends Herbivore implements Swim, Run, Voice {

    public Horse() {
    }

    public Horse(int satiety, String type) {
        super(satiety, type);
    }

    @Override
    public void run() {
        System.out.printf("The %s is galloping!\n", getType());
    }

    @Override
    public void swim() {
        System.out.printf("The %s is swim.\n", getType());
    }

    @Override
    public String voice() {
        return "Neigh-neigh!";
    }
}
