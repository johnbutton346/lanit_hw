package animals;

import food.Food;

public abstract class Animal {
    private int satiety;
    private String type;

    public Animal() {}

    public Animal(int satiety, String type) {
        this.satiety = satiety;
        this.type = type;
    }

    public abstract void eatFood(Food food);

    public int getSatiety() {
        return satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
