package animals;

public class Wolf extends Carnivorous implements Run, Voice, Swim {

    public Wolf() {
    }

    public Wolf(int satiety, String type) {
        super(satiety, type);
    }

    @Override
    public void run() {
        System.out.printf("The %s is running fast!\n", getType());
    }

    @Override
    public void swim() {
        System.out.printf("The %s is swim.\n", getType());
    }

    @Override
    public String voice() {
        return "АУФ!";
    }
}
