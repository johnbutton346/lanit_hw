package animals;

public class Duck extends Herbivore implements Voice, Run, Swim, Fly {

    public Duck() {
    }

    public Duck(int satiety, String type) {
        super(satiety, type);
    }

    @Override
    public void fly() {
        System.out.printf("The %s flies!\n", getType());
    }

    @Override
    public void run() {
        System.out.printf("The %s running to the pond.\n", getType());
    }

    @Override
    public void swim() {
        System.out.printf("The %s chill and swim!\n", getType());
    }

    @Override
    public String voice() {
        return "QUACK-QUACK!";
    }
}
