package animals;

public class Buffalo extends Herbivore implements Run, Swim, Voice {

    public Buffalo() {
    }

    public Buffalo(int satiety, String type) {
        super(satiety, type);
    }

    @Override
    public void run() {
        System.out.printf("The %s is running!", this.getType());
    }

    @Override
    public void swim() {
        System.out.printf("The %s is swim.\n", this.getType());
    }

    @Override
    public String voice() {
        return "MEGA-MOO!";
    }
}
