import animals.*;
import food.Food;
import food.Grass;
import food.Meat;

public class Zoo {
    public static void main(String[] args) {

        Worker worker = new Worker();

        Food grass = new Grass("Oats", 3);
        Food meat = new Meat("Ham", 5);

        Bear bear = new Bear(10, "Bear");
        Duck duck = new Duck(7, "Donald-Duck");
        Fish fish = new Fish(11, "Perch");
        Horse horse = new Horse(15, "Horse");
        Wolf wolf = new Wolf(4, "Wolf");
        Buffalo buffalo = new Buffalo(14, "Buffalo-Bill");

        Swim [] pond = { bear, duck, fish, horse, wolf, buffalo };


        worker.feed(bear, grass);
        worker.feed(bear, meat);
        worker.getVoice(bear);

        worker.feed(duck, grass);
        worker.feed(duck, meat);
        worker.getVoice(duck);

        worker.feed(fish, grass);
        worker.feed(fish, meat);

        worker.feed(wolf, grass);
        worker.feed(wolf, meat);
        worker.getVoice(wolf);

        worker.feed(buffalo, grass);
        worker.feed(buffalo, meat);
        worker.getVoice(buffalo);

        worker.feed(horse, grass);
        worker.feed(horse, meat);
        worker.getVoice(horse);

        for (Swim animal : pond) {
            animal.swim();
        }

    }
}
