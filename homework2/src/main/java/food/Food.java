package food;

public abstract class Food {

    private String name;
    private int nourishment;

    public Food() {
    }

    public Food(String name, int nourishment) {
        this.name = name;
        this.nourishment = nourishment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNourishment() {
        return nourishment;
    }

    public void setNourishment(int nourishment) {
        this.nourishment = nourishment;
    }

    @Override
    public String toString() {
        return name;
    }
}
