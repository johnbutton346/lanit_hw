package food;

public class Grass extends Food {

    public Grass() {
    }

    public Grass(String name, int nourishment) {
        super(name, nourishment);
    }
}
