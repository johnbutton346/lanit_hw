package model;

public class Kotik {

    private static int catCount = 0;

    private int satiety;

    private int prettiness;
    private String name;
    private String meow;
    private int weight;

    public Kotik() {
        catCount++;
    }

    public Kotik(int prettiness, String name, String meow, int weight) {
        this.prettiness = prettiness;
        this.name = name;
        this.meow = meow;
        this.weight = weight;
        this.satiety = 10;
        catCount++;
    }

    public void setKotik(int prettiness, String name, String meow, int weight) {
        setPrettiness(prettiness);
        setName(name);
        setMeow(meow);
        setWeight(weight);
        setSatiety(10);
    }

    public int getSatiety() {
        return satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeow() {
        return meow;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    public int countOfCats() {
        return catCount;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public boolean play() {
        if (this.satiety > 0) {
            this.satiety--;
            System.out.printf("I'am cat. My name is %s. And i am playing a great game! Catch my tail! WOW! IT'S SO INTERESTING!\n", name);
            return true;
        } else {
            return false;
        }
    }

    public boolean sleep() {
        if (this.satiety > 0) {
            this.satiety--;
            System.out.printf("I'am cat. My name is %s. And i am sleeping. *some sounds of sleep*\n", name);
            return true;
        } else {
            return false;
        }
    }

    public boolean chaseMouse() {
        if (this.satiety > 0) {
            this.satiety--;
            System.out.print("Hello! NOW I AM JOHNY CATVILLE NOW AND WE TRY TO CATCH THIS FREAKING MOUSE! OH MY GOD, HE IS RUNNING!\n");
            return true;
        } else {
            return false;
        }

    }

    public boolean purr() {
        if (this.satiety > 0) {
            this.satiety--;
            System.out.print("Purrrr..... purrr... purr...\n");
            return true;
        } else {
            return false;
        }
    }

    public boolean runLikeADevil() {
        if (this.satiety > 0) {
            this.satiety--;
            System.out.print("Oh, it's 4 am now? TIME IS RUN! RUN! RUN! RUN! RUN LIKE A HORSE! WOWWWWWWWWWWWWWWWO! *tygtyg sounds*\n");
            return true;
        } else {
            return false;
        }
    }

    public void eat(int satiety) {
        this.satiety += satiety;
        System.out.println("Eating! How delicious!");

    }

    public void eat(int satiety, String food) {
        this.satiety += satiety;
        System.out.printf("Eating %s! How delicious!\n", food);
    }

    public void eat() {
        eat(3, "Meet!");
    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            double randMethod = Math.random() * 5 + 1;
            switch ((int) randMethod) {
                case 1:
                    if (!play()) eat();
                    break;
                case 2:
                    if (!sleep()) eat();
                    break;
                case 3:
                    if (!chaseMouse()) eat();
                    break;
                case 4:
                    if (purr()) eat();
                    break;
                case 5:
                    if (!runLikeADevil()) eat();
                    break;
            }
        }
    }
}
