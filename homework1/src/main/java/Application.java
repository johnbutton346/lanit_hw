import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik boba = new Kotik(100, "BOBA", "MAOW!", 7);
        catInfo(boba);
        boba.liveAnotherDay();
        Kotik buba = new Kotik();
        buba.setKotik(95, "BUBA", "MAOW!", 10);
        System.out.println(boba.getMeow().equals(buba.getMeow()));
        System.out.printf("Current counts of cats: %d", boba.countOfCats());

    }

    public static void catInfo(Kotik cat) {
        System.out.print("Info about Cat:\n");
        System.out.printf("Name: %s\n", cat.getName());
        System.out.printf("Weight: %d kg\n", cat.getWeight());
        System.out.printf("Prettiness: %d%s\n", cat.getPrettiness(), " %");
    }
}
